#!/usr/bin/env bash

echo 'Creating application user(s) and db(s)'

mongo pluckin-server \
        --host localhost \
        --port 27017 \
        -u $MONGODB_PRIMARY_ROOT_USER \
        -p $MONGODB_ROOT_PASSWORD \
        --authenticationDatabase admin \
        --eval "db.createUser({user: 'pluckin-server', pwd: '$MONGODB_PASSWORD', roles:[{role:'dbOwner', db: 'pluckin-server'}]});"
