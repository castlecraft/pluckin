import { Test, TestingModule } from '@nestjs/testing';
import { StationPoliciesService } from './station-policies.service';

describe('StationPoliciesService', () => {
  let service: StationPoliciesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [StationPoliciesService],
    }).compile();

    service = module.get<StationPoliciesService>(StationPoliciesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
