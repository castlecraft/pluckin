import { IQuery } from '@nestjs/cqrs';
import { GetStationByLocationInterface } from '../../entity/station/station-by-location.interface';

export class RetrieveStationQuery implements IQuery {
  constructor(public readonly query: GetStationByLocationInterface) {}
}
