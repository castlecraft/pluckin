import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { RetrieveStationQuery } from './retrieve-station.query';
import { StationAggregateService } from '../../aggregates/station-aggregate/station-aggregate.service';

@QueryHandler(RetrieveStationQuery)
export class RetrieveStationHandler
  implements IQueryHandler<RetrieveStationQuery> {
  constructor(private readonly manager: StationAggregateService) {}

  async execute(retrieveStationQuery: RetrieveStationQuery) {
    const { query } = retrieveStationQuery;
    return this.manager.retrieveStationByLocation(query);
  }
}
