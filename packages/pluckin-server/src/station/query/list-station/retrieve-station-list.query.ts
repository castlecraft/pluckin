import { IQuery } from '@nestjs/cqrs';

export class RetrieveStationListQuery implements IQuery {
  constructor(
    public offset: number,
    public limit: number,
    public search: string,
    public sort: string,
  ) {}
}
