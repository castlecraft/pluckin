import { RetrieveStationHandler } from './get-station/retrieve-station.handler';
import { RetrieveStationListHandler } from './list-station/retrieve-station-list.handler';

export const StationQueryManager = [
  RetrieveStationHandler,
  RetrieveStationListHandler,
];
