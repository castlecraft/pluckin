import { Module, HttpModule } from '@nestjs/common';
import { StationAggregatesManager } from './aggregates';
import { StationEntitiesModule } from './entity/entity.module';
import { StationQueryManager } from './query';
import { CqrsModule } from '@nestjs/cqrs';
import { StationCommandManager } from './command';
import { StationEventManager } from './event';
import { StationController } from './controllers/station/station.controller';
import { StationPoliciesService } from './policies/station-policies/station-policies.service';

@Module({
  imports: [StationEntitiesModule, CqrsModule, HttpModule],
  controllers: [StationController],
  providers: [
    ...StationAggregatesManager,
    ...StationQueryManager,
    ...StationEventManager,
    ...StationCommandManager,
    StationPoliciesService,
  ],
  exports: [StationEntitiesModule],
})
export class StationModule {}
