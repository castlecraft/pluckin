import { Test, TestingModule } from '@nestjs/testing';
import { StationAggregateService } from './station-aggregate.service';
import { StationService } from '../../entity/station/station.service';

describe('StationAggregateService', () => {
  let service: StationAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        StationAggregateService,
        {
          provide: StationService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<StationAggregateService>(StationAggregateService);
  });
  StationAggregateService;
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
