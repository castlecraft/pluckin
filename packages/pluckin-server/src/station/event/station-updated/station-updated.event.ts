import { IEvent } from '@nestjs/cqrs';
import { Station } from '../../entity/station/station.entity';

export class StationUpdatedEvent implements IEvent {
  constructor(public updatePayload: Station) {}
}
