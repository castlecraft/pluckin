import { StationAddedHandler } from './station-added/station-added.handler';
import { StationRemovedHandler } from './station-removed/station.removed.handler';
import { StationUpdatedHandler } from './station-updated/station-updated.handler';

export const StationEventManager = [
  StationAddedHandler,
  StationRemovedHandler,
  StationUpdatedHandler,
];
