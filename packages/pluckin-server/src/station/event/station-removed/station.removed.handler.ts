import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { StationService } from '../../entity/station/station.service';
import { StationRemovedEvent } from './station-removed.event';

@EventsHandler(StationRemovedEvent)
export class StationRemovedHandler
  implements IEventHandler<StationRemovedEvent> {
  constructor(private readonly stationService: StationService) {}

  async handle(event: StationRemovedEvent) {
    const { station } = event;
    await this.stationService.deleteOne({ uuid: station.uuid });
  }
}
