import { IEvent } from '@nestjs/cqrs';
import { Station } from '../../entity/station/station.entity';

export class StationAddedEvent implements IEvent {
  constructor(public station: Station, public clientHttpRequest: any) {}
}
