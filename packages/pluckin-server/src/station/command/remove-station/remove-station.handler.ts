import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { RemoveStationCommand } from './remove-station.command';
import { StationAggregateService } from '../../aggregates/station-aggregate/station-aggregate.service';

@CommandHandler(RemoveStationCommand)
export class RemoveStationHandler
  implements ICommandHandler<RemoveStationCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: StationAggregateService,
  ) {}
  async execute(command: RemoveStationCommand) {
    const { uuid } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await this.manager.remove(uuid);
    aggregate.commit();
  }
}
