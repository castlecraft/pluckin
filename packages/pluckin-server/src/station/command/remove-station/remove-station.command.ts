import { ICommand } from '@nestjs/cqrs';

export class RemoveStationCommand implements ICommand {
  constructor(public readonly uuid: string) {}
}
