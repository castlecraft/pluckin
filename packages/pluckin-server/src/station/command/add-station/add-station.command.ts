import { ICommand } from '@nestjs/cqrs';
import { StationDto } from '../../entity/station/station-dto';

export class AddStationCommand implements ICommand {
  constructor(
    public userPayload: StationDto,
    public readonly clientHttpRequest: any,
  ) {}
}
