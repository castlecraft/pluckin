import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { AddStationCommand } from './add-station.command';
import { StationAggregateService } from '../../aggregates/station-aggregate/station-aggregate.service';

@CommandHandler(AddStationCommand)
export class AddStationHandler implements ICommandHandler<AddStationCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: StationAggregateService,
  ) {}
  async execute(command: AddStationCommand) {
    const { userPayload, clientHttpRequest } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.addStation(userPayload, clientHttpRequest);
    aggregate.commit();
  }
}
