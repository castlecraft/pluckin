import {
  Controller,
  Post,
  UseGuards,
  UsePipes,
  Body,
  ValidationPipe,
  Req,
  Param,
  Get,
  Query,
  BadRequestException,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { StationDto } from '../../entity/station/station-dto';
import { AddStationCommand } from '../../command/add-station/add-station.command';
import { RemoveStationCommand } from '../../command/remove-station/remove-station.command';
import { UpdateStationCommand } from '../../command/update-station/update-station.command';
import { Station } from '../../entity/station/station.entity';
import { RetrieveStationQuery } from '../../query/get-station/retrieve-station.query';
import { RetrieveStationListQuery } from '../../query/list-station/retrieve-station-list.query';
import { GetStationByLocationInterface } from '../../entity/station/station-by-location.interface';
import { throwError } from 'rxjs';
import { LAT_LONG_VALIDATION_MESSAGE } from '../../../constants/messages';

@Controller('station')
export class StationController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Post('v1/create')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  create(@Body() Payload: StationDto, @Req() req) {
    return this.commandBus.execute(new AddStationCommand(Payload, req));
  }

  @Post('v1/remove/:uuid')
  @UseGuards(TokenGuard)
  remove(@Param('uuid') uuid) {
    return this.commandBus.execute(new RemoveStationCommand(uuid));
  }

  @Post('v1/update')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  updateClient(@Body() updatePayload: Station) {
    return this.commandBus.execute(new UpdateStationCommand(updatePayload));
  }

  @Get('v1/get')
  @UseGuards(TokenGuard)
  getByLocation(
    @Query('lat') lat: number,
    @Query('long') long: number,
    @Query('maxDistance') maxDistance: number = 500,
    @Query('minDistance') minDistance: number = 0,
  ) {
    lat = Number(lat);
    long = Number(long);
    if (!lat || !long || typeof lat !== 'number' || typeof lat !== 'number') {
      return throwError(new BadRequestException(LAT_LONG_VALIDATION_MESSAGE));
    }
    const query: GetStationByLocationInterface = {
      coordinates: [lat, long],
      maxDistance: maxDistance > 0 ? maxDistance : 500,
      minDistance: minDistance > 0 ? maxDistance : 0,
    };
    return this.queryBus.execute(new RetrieveStationQuery(query));
  }

  @Get('v1/list')
  @UseGuards(TokenGuard)
  listRole(
    @Query('start') offset = 0,
    @Query('limit') limit = 10,
    @Query('search') search = '',
    @Query('sort') sort,
  ) {
    if (sort !== 'ASC') {
      sort = 'DESC';
    }
    return this.queryBus.execute(
      new RetrieveStationListQuery(offset, limit, search, sort),
    );
  }
}
