export interface GetStationByLocationInterface {
  coordinates: number[];
  maxDistance: number;
  minDistance: number;
}
