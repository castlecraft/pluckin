import { Column, ObjectIdColumn, BaseEntity, ObjectID, Entity } from 'typeorm';

export class MongoGeoLocation {
  type: string;
  coordinates: number[];
}

@Entity()
export class Station extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectID;

  @Column()
  uuid: string;

  @Column()
  name: string;

  @Column()
  address: string;

  @Column()
  totalPlucks: number;

  @Column()
  availablePlucks: number;

  @Column()
  rate: number;

  @Column()
  location: MongoGeoLocation;
}
