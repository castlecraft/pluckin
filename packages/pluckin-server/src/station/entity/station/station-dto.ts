import {
  IsString,
  IsNotEmpty,
  IsNumber,
  ValidateNested,
  IsIn,
} from 'class-validator';
import { Type } from 'class-transformer';

export const MongoGeoLocationType = ['Point'];

export class MongoGeoLocationInterface {
  @IsNotEmpty()
  @IsIn(MongoGeoLocationType)
  type: string;

  @IsNotEmpty()
  @IsNumber({}, { each: true })
  coordinates: number[];
}

export class StationDto {
  @IsString()
  @IsNotEmpty()
  name: string;

  @IsString()
  @IsNotEmpty()
  address: string;

  @IsNumber()
  @IsNotEmpty()
  totalPlucks: number;

  @IsNumber()
  @IsNotEmpty()
  availablePlucks: number;

  @IsNumber()
  @IsNotEmpty()
  rate: number;

  @IsNotEmpty()
  @ValidateNested()
  @Type(() => MongoGeoLocationInterface)
  location: MongoGeoLocationInterface;
}
