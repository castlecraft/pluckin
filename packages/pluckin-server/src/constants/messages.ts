export const SETUP_ALREADY_COMPLETE = 'Setup already complete';
export const PLEASE_RUN_SETUP = 'Please run setup';
export const SOMETHING_WENT_WRONG = 'Something went wrong';
export const NOT_CONNECTED = 'not connected';
export const SERVICE_ALREADY_REGISTERED = 'Service already registered';
export const INVALID_CODE = 'Invalid Code';
export const INVALID_STATE = 'Invalid State';
export const MONGO_LOCATION_INDEX_SUCCESSFUL =
  'Mongo location index successful';
export const MONGO_LOCATION_INDEX_FAILURE = 'Mongo location index Failed';
export const LAT_LONG_VALIDATION_MESSAGE =
  'Lat Long are mandatory and should be valid number';
