export CHECK_MM=$(helm ls -q stg-pluckin-server --tiller-namespace production)
if [ "$CHECK_MM" = "stg-pluckin-server" ]
then
    echo "Updating existing stg-pluckin-server . . ."
    helm upgrade stg-pluckin-server \
        --tiller-namespace production \
        --namespace production \
        --reuse-values \
        helm-charts/pluckin
fi
